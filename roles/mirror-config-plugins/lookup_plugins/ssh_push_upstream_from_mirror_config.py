from ansible.errors import AnsibleUndefinedVariable
from ansible.plugins.lookup import LookupBase
from ansible.template import Templar


_marker = object()


class LookupModule(LookupBase):
    def lookup(self, name, hostname, variables, entry=None, default=_marker):
        hostvars = variables['hostvars']

        if not hostname in hostvars:
            if default is not _marker:
                return default
            return None

        v = {
                i: variables[i]
                for i in variables
                if i.startswith('ansible_') or i == 'hostvars' }
        v.update(hostvars[hostname])

        try:
            if entry:
                templ = entry[name]
            else:
                templ = v[name]
        except KeyError:
            if default is not _marker:
                return default
            raise

        templar = Templar(self._loader)
        templar.set_available_variables(v)
        return templar.template(templ, fail_on_undefined=True)

    def run(self, terms, variables=None, **kwargs):
        hostname = variables.get('inventory_hostname')
        if not hostname:
            return []

        groups = variables['groups']

        config = self.lookup('mirror_config', hostname, variables, default={})

        ret = set()

        for name, mirror in config.items():
            upstream = mirror.get('upstream')
            if not upstream:
                continue
            if upstream in groups:
                ret.update(groups[upstream])
            elif upstream in groups['all']:
                ret.add(upstream)
            else:
                raise AnsibleError('Unable to find upstream {} for {}:{}'
                    .format(upstream, hostname, name))

        return [sorted(list(ret))]
