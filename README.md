# Setup for Debian operated mirrors

## Inventory

### Debian project mirrors (`inventory/debian`)

This inventory file specifies all mirrors maintained by the Debian mirror team.
Most of them run on `debian.org` hosts.

Groups are used to assign service names and roles to mirrors.

`*-master.debian.org`
: All the masters.

`syncproxy.*.debian.org`
: All the syncproxies

`anycast-test.mirrors.debian.org`
: All mirrors that are part of the anycast test.

`security.debian.org`
: All mirrors that are part of the `security.debian.org` rotation.

`debian.org`
: All mirrors on `debian.org` systems.

`debian`
: All mirrors run by the mirror team.

### Debian project cloud mirrors (`inventory/cloud.yaml`, `inventory/libcloud.yaml`)

The inventory file specifies the fixed groups as base for all cloud systems.
The second file is the config file for the [libcloud inventory plugin](https://salsa.debian.org/mirror-team/ansible-inventory-libcloud).

The following groups are defined.

`cloud-backend`
: All HTTP backends on cloud systems.

`cloud-syncproxy`
: All syncproxies on cloud systems.

Systems are imported using a inventory plugin from the following infrastructures:

- Google Cloud, project `debian-infra` for systems under `*.mirror.gce.debian.org`.

### External mirrors (`inventory/external`)

This file specifies two groups per syncproxy.
The group named `external-$name` allows access to the `debian` repo.
The group named `external-$name+security` allows access to the `debian` and `debian-security` repo.
Other access variants needs a complete mirror spec in `host_vars/$hostname.yaml`.

Each external mirror definition enables both rsync access and ssh push.
The hostname is also used as rsync user name.
Push is configured with a set of extra variables:

`mirror_upstream_push`
: Set to `False` to disable push altogether.

`mirror_upstream_push_host`
: Specify a different hostname for the ssh connection.

`mirror_upstream_push_user`
: Specify the user for the ssh connection.

`mirror_upstream_push_params`
: Specify extra parameters for the ssh connection.

`mirror_upstream_push_delay`
: The absolute amount of time the push should be delayed.
  It is automatically converted into `DELAY` definitions in the runmirrors list.

A static password needs to be defined in the extra password git.

## Playbooks

### site.playbook

Sets up archvsync (ftpsync, runmirrors), rsync secrets and ssh ``authorized_keys`` on all managed mirrors.

### mirrorinfo.playbook

Create a sheet with information for each external mirror including password, hosts to pull from, ssh ``authorized_keys`` and IP of the upstreams.
